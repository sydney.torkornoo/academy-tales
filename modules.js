module.exports.getDatas = async function (req, res, next) {
	ref.child('/Users').on('value', function(snapshot) {
  		req.data = snapshot.val() ;
		next();
	});
}

module.exports.getPastors = async function (req, res, next) {
	ref.child('/Pastors').on('value', function(snapshot) {
  		req.data = snapshot.val();
		next();
	});
}

module.exports.getUserDetail = async function (req, res, next) {
	ref.child('/Users/'+req.params.detail).on('value', function(snapshot) {
  		req.data = snapshot.val() ;
		next();
	});
}

module.exports.getSermons = async function (req, res, next) {
	// console.log(req.params.detail);
	// return false;
	ref.child('/Sermons').on('value', function(snapshot) {
  		req.data = snapshot.val() ;
		next();
	});
}

module.exports.getDevotional = async function (req, res, next) {
	// console.log(req.params.detail);
	// return false;
	ref.child('/Devotionals').on('value', function(snapshot) {
  		req.data = snapshot.val() ;
		next();
	});
}

module.exports.processForm = async function (req, res, next) {
	var form = new multiparty.Form();
	form.parse(req, function(err, fields, files) {
		console.log(fields);
		console.log(files);
    });
	return false;
	next();
}
M.AutoInit();
$('.modal-open-btn').on('click', function(e) {
	e.preventDefault();
	let target = $(this).data('target')
	setTimeout(function() {
		let i = M.Modal.getInstance($('#'+target))
		i.open()
	}, 200)
})
let user = 'student'
$('.user-type').on('click', function(e) {
	e.preventDefault()
	$('.more-info').addClass('hidden')
	user = $(this).data('user')
	$('.more-info[data-user="'+user+'"]').removeClass('hidden')
	$('.user-type').removeClass('blue')
	$('.user-type').removeClass('white-text')
	$('.user-type').addClass('white')
	$('.user-type').addClass('blue-text')

	$(this).addClass('blue')
	$(this).addClass('white-text')
	$(this).removeClass('white')
	$(this).removeClass('blue-text')
})

function signOut() {
	login = 1;
	firebase.auth().signOut()
}

$().ready(function() {
})
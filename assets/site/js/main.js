
var provider = new firebase.auth.GoogleAuthProvider();

function gSignIn() {
	firebase.auth().signInWithRedirect(provider).then(function(result) {
	  // This gives you a Google Access Token. You can use it to access the Google API.
	  var token = result.credential.accessToken;
	  // The signed-in user info.
	  var user = result.user;
	  // ...
	  console.log(user)
	}).catch(function(error) {
	  // Handle Errors here.
	  var errorCode = error.code;
	  var errorMessage = error.message;
	  // The email of the user's account used.
	  var email = error.email;
	  // The firebase.auth.AuthCredential type that was used.
	  var credential = error.credential;
	  // ...
	  console.log(errorMessage)
	});
}

var ui = new firebaseui.auth.AuthUI(firebase.auth());
var uiConfig = {
  callbacks: {
    signInSuccessWithAuthResult: function(authResult, redirectUrl) {
	    let ix = M.Modal.getInstance($('#modal-sign-in'))
		ix.close();
		firebase.firestore().collection('users').doc(authResult.user.uid).get()
        .then(function(doc) {
        	if (!doc.exists) {
        		firebase.firestore().collection('users').doc(authResult.user.uid).set({
        			user: user
        		})
        	} else {
        		$('.dashboard').attr('href', '/open-school/dashboard/'+user)
        	}
        })
        .catch(function(e) {
        	alert('error')
        	console.log(e)
        })
     	return false;
    },
  signInSuccess: function(currentUser, credential, redirectUrl) {
	  return false;
	},
    uiShown: function() {
      document.getElementById('loader').style.display = 'none';
    }
  },
  signInFlow: 'popup',
  // signInSuccessUrl: '/my-account',
  signInOptions: [
    // Leave the lines as is for the providers you want to offer your users.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    // firebase.auth.EmailAuthProvider.PROVIDER_ID
    // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    // firebase.auth.GithubAuthProvider.PROVIDER_ID,
    // firebase.auth.PhoneAuthProvider.PROVIDER_ID
  ],
  // Terms of service url.
  tosUrl: '<your-tos-url>',
  // Privacy policy url.
  privacyPolicyUrl: '<your-privacy-policy-url>'
};
let login = 0
ui.start('#firebaseui-auth-container', uiConfig);
let _user = $('[name="sign_in"]').val()
firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
		_user = user.uid;
		$('[name="sign_in"]').val(_user)
        $('.logged-in').removeClass('hidden')
        $('.not-logged-in').addClass('hidden')
        firebase.firestore().collection('users').doc(user.uid).get()
        .then(function(doc) {
        	if (doc.exists) {
        		$('.dashboard').attr('href', '/open-school/dashboard/'+doc.data().user)
        	}
        })
	} else {
        $('.logged-in').addClass('hidden')
        $('.not-logged-in').removeClass('hidden')
        setTimeout(function() {
        	if (login == 0) {
	        	let ix = M.Modal.getInstance($('#modal-sign-in')).open();
        	}
        }, 5000)
	}
});

$('[name="sign_in"]').on('change', function() {
	console.log($(this).val())
	_user = $(this).val()
})
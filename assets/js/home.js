function getProgrammes(limit) {
	firebase.firestore().collection('programmes').get().then(function(snap) {
		let s = 0
      snap.forEach(function(v, i) {
      	s = 1
        // let _this = Object.values(v._document.proto)[0].substring(Object.values(v._document.proto)[0].indexOf('depot/') + 'depot/'.length, Object.values(v._document.proto)[0].length)
        // console.log(_this)
        let tr  = 
        		`<div>
	                <div class="uk-card uk-card-default uk-margin-remove uk-padding-remove uk-box-shadow-medium uk-box-shadow-hover-xlarge">
                        <a href="/programme/${v.data().name.replace(' ', '-').toLowerCase()}">
                        <div class="uk-card-header uk-padding-remove">
                        	<div class="uk-height-small uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light" data-src="${v.data().image}" uk-img>
							</div>
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-text-capitalize" style="color: rgb(4, 64, 98);">${v.data().name}</h3>
                            <a href="/programme/${v.data().name.replace(' ', '-').toLowerCase()}" class="uk-float-right uk-button-text uk-text-primary uk-text-capitalize" style="position: absolute; bottom: 10px; right: 10px">View More <span uk-icon="icon: arrow-right"></span></a>
                        </div>
                        </a>
                    </div>
                </div>`
        $('.home-programmes').append(tr)
      })
      return s
    }).then(function(s) {
    	$('.uk-progress').val(100)
    	$('.preload-screen').delay(500).fadeOut(200)
    	$('body').css('overflow', 'auto')
    	if (s == 1) {
	    	$('.preloader').addClass('uk-hidden')
    	}
    })
}

function signOut() {
	firebase.auth().signOut()
}

function getSubjects(type) {}

$().ready(function() {
	getProgrammes()
})
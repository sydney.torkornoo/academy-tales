// Initialize Firebase
let db = firebase.database();
// let courses = db.ref().child('courses');
let subjects = db.ref().child('subjects');
let fs = firebase.firestore()

function getCourses() {

    firebase.firestore().collection('programmes').get().then(function(snap) {
      let counter = 0
      snap.forEach(function(v) {
        let d = v.data()
        let img
        if (d.image) {
          img = d.image
        } else {
          img = '../assets/img/logo.png'
        }
        let programme = `<tr>
	                		<td><img src="${img}" style="height: 60px;" class="uk-border-circle"></td>
	                		<td>${d.name}</td>
	                		<td>9034</td>
	                		<td><span uk-icon="icon: star"></span><span uk-icon="icon: star"></span><span uk-icon="icon: star"></span></td>
	                		<td>
		                   		 <a href="programme/${v.id}" class="uk-button uk-button-default uk-border-rounded" style="padding: 5px 10px;"><span uk-icon="icon: info"></span></a>
		                   		 <button class="uk-button uk-button-primary uk-border-rounded" style="padding: 5px 10px;"><span uk-icon="icon: pencil"></span></button>
		                   		 <button class="uk-button uk-button-danger uk-border-rounded" style="padding: 5px 10px;"><span uk-icon="icon: ban"></span></button>
		               		 </td>
		           		 </tr>`;
		$('tbody[id="course_body"]').prepend(programme);
      })
	})
}

$().ready(function() {
	getCourses();
	let id;

	$('form').on('submit', function (e) {
		// body...
		e.preventDefault();
	});

	var bar = document.getElementById('js-progressbar')
	var bar2 = document.getElementById('js-progressbar2')

	$('[id="add-teacher-form"] input[name="name"]').on('keyup', function() {
        $('[id="add-teacher-form"] input[name="email"]').val($('[id="add-teacher-form"] input[name="name"]').val().replace(/\s/g, '.').toLowerCase() +'@academytales.com')
    })

	$('#add-topic-form').on('submit', function(e) {
		let _name = $(this).find('input[name="topic"]').val();
		let _desc = $(this).find('textarea[name="description"]').val();
		let data = {
			name : _name,
			description: _desc
		};
		let sub = db.ref().child('subjects/'+id+'/topics').push();
		sub.set(data);	
	});

	firebase.firestore().collection('cores').get().then(function(collection) {
		collection.forEach(function(doc) {
	        // doc.data() is never undefined for query doc snapshots
	        $('.add-teacher-subjects-list').append(`<label><input class="uk-checkbox uk-text-capitalize" type="checkbox" name="subjects-list" value="${doc.id}"> ${doc.id.replace(/-/g, ' ')}</label>`)
	    });
	})

	firebase.firestore().collection('subjects').get().then(function(collection) {
		collection.forEach(function(doc) {
	        // doc.data() is never undefined for query doc snapshots
	        $('.add-teacher-subjects-list').append(`<label><input class="uk-checkbox uk-text-capitalize" type="checkbox" name="subjects-list" value="${doc.id}"> ${doc.id}</label>`)
	    });
	})

	let video_file = null, image_file = null, videolink = ''

	$('input[name="image"]').on('change', function(e) {
		image_file = e.target.files[0]
	})

	$('input[name="video"]').on('change', function(e) {
		video_file = e.target.files[0]
	})

	$('[id="add-teacher-form"]').on('submit', function() {
		let __data = {}, subjects = [], phone = ''
		__data.verified = false
		$(this).serializeArray().forEach(function(v) {
			if (v.name == 'subjects-list') {
				subjects.push(v.value) 
			} else {
				__data[v.name] = v.value
				if (v.name == 'phone') {
					phone = v.value
				}
			}
			__data['subjects'] = subjects
		})
		console.log(__data)
		firebase.firestore().collection('teachers').doc(phone).set(__data)
		.then(function() {
			alert('teacher added successfully')
			UIkit.modal($('#add-teacher')).hide()
		})
		.catch(function() {
			alert('teacher addition false')
			UIkit.modal($('#add-teacher')).hide()
		})
		return
	})

	$('form[id="add-programme"]').on('submit', function(e) {
		e.preventDefault()
		if (image_file != null && video_file != null ) {
			let storageRef = firebase.storage().ref('prog_desc/' + image_file.name)
			let storageRef2 = firebase.storage().ref('prog_desc/' + video_file.name)
			let upstats = storageRef.put(image_file)
			let upstats2 = storageRef2.put(video_file)
			upstats.on('state_changed', 
				function progress(snapshot) {
					var p = snapshot.bytesTransferred / snapshot.totalBytes * 45
					bar.value = p
				},
				function error(e) {},
				function complete() {
					upstats.snapshot.ref.getDownloadURL().then(function(downloadURL) {
						videolink = downloadURL
						upstats2.on('state_changed', 
							function progress(snapshot) {
								var p = snapshot.bytesTransferred / snapshot.totalBytes * 45
								bar.value = 50 + p
							},
							function error(e) {},
							function complete() {
								upstats2.snapshot.ref.getDownloadURL().then(function(downloadURL) {
									console.log(downloadURL)
									console.log(videolink)
									let data = {
										video : downloadURL,
										image : videolink,
										desc : $('form[id="add-programme"]').find('textarea').val(),
										name : $('form[id="add-programme"]').find('input[name="name"]').val()
									}
									fs.collection("programmes").doc($('form[id="add-programme"]').find('input[name="name"]').val().replace(/-/g, '-').toLowerCase()).set(data, { merge: true })
									.then(function() {
										bar.value = 100
										videolink = video_file = image_file = null
										setTimeout(function() {
											UIkit.modal($('#add-programme')).hide()
											bar.value = 0
											$('form[id="add-programme"]').find('button, input, textarea').attr('disabled', false)
											$('form[id="add-programme"]').find('input, textarea').val('')
										    $('.uk-alert-success').find('p').text("Programme successfully added!")
										    $('.uk-alert-success').removeClass('uk-hidden')
										    setTimeout(function() {
										    	$('.uk-alert-success').remove()
										    }, 3000)
										}, 1000)
									})
								})
							}
						)
					})
				}
			)
		} else {
			$('.uk-alert-danger').find('p').text("Error Uploading: Some files missing. Select all required files and try again!")
		    $('.uk-alert-danger').removeClass('uk-hidden')
		    setTimeout(function() {
		    	$('.uk-alert-danger').addClass('uk-hidden')
		    }, 3000)
		}
	});

	$('form[id="add-subject"]').on('submit', function(e) {
		e.preventDefault()
		// bar2.value = 100
		// return
		let collection = 'subjects'
		if($('input[name="core_subject"]').prop('checked')) {
			collection = 'cores'
		}
		// console.log(image_file)
		// console.log(video_file)
		// return
		if (image_file != null && video_file != null && $(this).find('input[name="image"]').val() != null && $(this).find('input[name="video"]').val() != null) {
			// console.log(true)
			// return
			let storageRef = firebase.storage().ref('subject/' + image_file.name)
			let storageRef2 = firebase.storage().ref('subject/' + video_file.name)
			let upstats = storageRef.put(image_file)
			let upstats2 = storageRef2.put(video_file)
			upstats.on('state_changed', 
				function progress(snapshot) {
					var p = snapshot.bytesTransferred / snapshot.totalBytes * 45
					bar2.value = p
				},
				function error(e) {},
				function complete() {
					upstats.snapshot.ref.getDownloadURL().then(function(downloadURL) {
						videolink = downloadURL
						upstats2.on('state_changed', 
							function progress(snapshot) {
								var p = snapshot.bytesTransferred / snapshot.totalBytes * 45
								bar2.value = 50 + p
							},
							function error(e) {},
							function complete() {
								upstats2.snapshot.ref.getDownloadURL().then(function(downloadURL) {
									console.log(downloadURL)
									console.log(videolink)
									let data = {
										description : {
											video : downloadURL,
											image : videolink,
											desc : $('form[id="add-subject"]').find('textarea').val(),
											name : $('form[id="add-subject"]').find('input[name="name"]').val()
										}
									}
									fs.collection(collection).doc($('form[id="add-subject"]').find('input[name="name"]').val().replace(/\s/g, '-').toLowerCase()).set(data, { merge: true })
									.then(function() {
										bar2.value = 100
										setTimeout(function() {
											UIkit.modal($('#add-subject-modal')).hide()
											bar2.value = 0
											$('form[id="add-subject"]').find('button, input, textarea').attr('disabled', false)
											$('form[id="add-subject"]').find('input, textarea').val('')
										    $('.uk-alert-success').find('p').text("Subject successfully added!")
										    $('.uk-alert-success').removeClass('uk-hidden')
										    setTimeout(function() {
										    	$('.uk-alert-success').addClass('uk-hidden')
										    }, 3000)
										}, 1000)
									})
								})
							}
						)
					})
				}
			)
		} else {
			$('.uk-alert-danger').find('p').text("Error Uploading: Some files missing. Select all required files and try again!")
		    $('.uk-alert-danger').removeClass('uk-hidden')
		    setTimeout(function() {
		    	$('.uk-alert-danger').addClass('uk-hidden')
		    }, 3000)
		}
	});

});
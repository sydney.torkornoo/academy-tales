
var provider = new firebase.auth.GoogleAuthProvider();

function gSignIn() {
	firebase.auth().signInWithRedirect(provider).then(function(result) {
	  // This gives you a Google Access Token. You can use it to access the Google API.
	  var token = result.credential.accessToken;
	  // The signed-in user info.
	  var user = result.user;
	  // ...
	  console.log(user)
	}).catch(function(error) {
	  // Handle Errors here.
	  var errorCode = error.code;
	  var errorMessage = error.message;
	  // The email of the user's account used.
	  var email = error.email;
	  // The firebase.auth.AuthCredential type that was used.
	  var credential = error.credential;
	  // ...
	  console.log(errorMessage)
	});
}

var ui = new firebaseui.auth.AuthUI(firebase.auth());
var uiConfig = {
  callbacks: {
    signInSuccessWithAuthResult: function(authResult, redirectUrl) {
      // User successfully signed in.
      // window.location.href = '/my-account'
      // Return type determines whether we continue the redirect automatically
      // or whether we leave that to developer to handle.
      UIkit.modal($('#modal-full')).hide()
      // console.log('authResult')
      // console.log(authResult)
      return false;
    },
 //  signInSuccess: function(currentUser, credential, redirectUrl) {
	//   const userId = currentUser.uid; 
	//   // Manually redirect.
	//   console.log('currentUser')
	//   console.log(currentUser)
	//   // redirectUrl = '/my-account'
 //      // window.location.href = '/my-account'
	//   // Do not automatically redirect.
	//   return false;
	// },
    uiShown: function() {
      // The widget is rendered.
      // Hide the loader.
      document.getElementById('loader').style.display = 'none';
    }
  },
  // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
  signInFlow: 'popup',
  // signInSuccessUrl: '/my-account',
  signInOptions: [
    // Leave the lines as is for the providers you want to offer your users.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID
    // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    // firebase.auth.GithubAuthProvider.PROVIDER_ID,
    // firebase.auth.PhoneAuthProvider.PROVIDER_ID
  ],
  // Terms of service url.
  tosUrl: '<your-tos-url>',
  // Privacy policy url.
  privacyPolicyUrl: '<your-privacy-policy-url>'
};
ui.start('#firebaseui-auth-container', uiConfig);
firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
		// console.log(user.uid)
        $('.logged-in').removeClass('uk-hidden')
        $('.not-logged-in').addClass('uk-hidden')
        firebase.firestore().collection('users').doc(user.uid).get()
        .then(function(doc) {
        	if (doc.exists) {
		      if ($('#modal-full').length == 0) {
		      	window.location.href = '/me/class'
		      } else {
			      UIkit.modal($('#modal-full')).hide()
			      if (! $('.preload-screen').hasClass('uk-hidden')) {
						$('.uk-progress').val(100)
						$('.preload-screen').delay(500).fadeOut(200)
						$('body').css('overflow', 'auto')
					}
		      }
        	} else {
        		if (window.location.href.indexOf('/me/set-up-profile') <= 0) {
	        		window.location.href = '/me/set-up-profile'
        		} else {
        			if (! $('.preload-screen').hasClass('uk-hidden')) {
						$('.uk-progress').val(100)
						$('.preload-screen').delay(500).fadeOut(200)
						$('body').css('overflow', 'auto')
						
					}
        		}
        		// alert('set up profile now')
        	}
        })
		// alert()
		// console.log(user)
	  // document.getElementById('quickstart-sign-in-status').textContent = 'Signed in';
	  // $('#myModal').modal({
	  //   backdrop: 'static',
	  //   keyboard: false
	  // })
	  // $('#myModal').modal('show')
	  // setTimeout(function(){ window.location.href = '/dashboard' }, 1000);
	  //
	  // console.log(user.uid)
	  // firebase.firestore().collection('admins').doc(user.uid).get()
	  // .then(function(snap) {
	  //   if(snap.data().superAdmin) {
	  //     window.location.href = '/dashboard'
	  //   } else {
	  //     window.location.href = '/approvals' 
	  //   }
	  // })
	  // User is signed in.
	  // return
	  // var displayName = user.displayName;
	  // var email = user.email;
	  // var emailVerified = user.emailVerified;
	  // var photoURL = user.photoURL;
	  // var isAnonymous = user.isAnonymous;
	  // var uid = user.uid;
	  // var providerData = user.providerData;
	  // // [START_EXCLUDE]
	  // document.getElementById('quickstart-sign-in').textContent = 'Sign out';
	  // document.getElementById('quickstart-account-details').textContent = JSON.stringify(user, null, '  ');
	  // [END_EXCLUDE]
	} else {
        $('.logged-in').addClass('uk-hidden')
        $('.not-logged-in').removeClass('uk-hidden')
        if (! $('.preload-screen').hasClass('uk-hidden')) {
			$('.uk-progress').val(100)
			$('.preload-screen').delay(500).fadeOut(200)
			$('body').css('overflow', 'auto')
			
		}
	  // User is signed out.
	  // [START_EXCLUDE]
	  // document.getElementById('quickstart-sign-in-status').textContent = 'Signed out';
	  // document.getElementById('quickstart-sign-in').textContent = 'Sign in with Google';
	  // document.getElementById('quickstart-account-details').textContent = 'null';
	  // document.getElementById('quickstart-oauthtoken').textContent = 'null';
	  // [END_EXCLUDE]
	}
	// [START_EXCLUDE]
	// document.getElementById('quickstart-sign-in').disabled = false;
	// [END_EXCLUDE]
});
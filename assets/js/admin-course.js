function populateSubjects() {
	firebase.firestore().collection('programmes').doc($('body').data('programme')).get()
	.then(function(doc) {
		for(var key in doc.data().subjects) {
			$('[id="programme_subjects"]').append(`<div>
                            <div class="uk-card uk-card-default uk-box-shadow-large uk-box-shadow-hover-small">
                                <div class="uk-card-body">
                                    <a href="/admin/subjects/${key.replace(/\s/g, '-').toLowerCase()}"><h3 class="uk-margin-remove uk-padding-small uk-button uk-button-text uk-card-title uk-text-capitalize">${key.replace(/-/g, ' ')}</h3></a>
                                </div>
                            </div>
                        </div>`)
		}
	})	
}

firebase.firestore().collection('subjects').get().then(function(collection) {
	collection.forEach(function(doc) {
        // doc.data() is never undefined for query doc snapshots
        $('.subjects').append(`<label><input class="uk-checkbox uk-text-capitalize" type="checkbox" name="subjects-list" value="${doc.id.replace(/\s/g, '-').toLowerCase()}"> ${doc.id}</label>`)
    });
})

$('[id="add_subject"]').on('submit', function(e) {
	e.preventDefault()
	// console.log($(this).serializeArray())
	var data = {}
	$(this).serializeArray().forEach(function(_data) {
		data[_data.value] = true
	})
	firebase.firestore().collection('programmes').doc($('body').data('programme')).update({subjects : data})
	.then(function() {
		// console.log()
		UIkit.modal($('[id="add-course"]')).hide()
		$('[id="add_subject"]').find('input').prop('checked', false)
	})
})

$().ready(function() {
	populateSubjects()
})
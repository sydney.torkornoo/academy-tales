const https = require('https');
const fs = require('fs');
let express = require('express');
// var firebaseapp = require('firebase');
// let admin = require('firebase-admin');
// var serviceAccount = require("./ChurchcastDummy-90509a45e820.json");
var bodyParser = require('body-parser');
var multiparty = require('multiparty');
var request = require('request');

let app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');
app.use('/assets', express.static('assets'));

async function processForm(req, res, next) {
	var form = new multiparty.Form();
	form.parse(req, function(err, fields, files) {
		req.formData = fields;
		console.log(req.formData);
		// next();
    });
	next();
}

function getImageLink (req, res, next) {

    var form = new multiparty.Form();

    form.on("part", function(part) {
        if(part.filename)
        {
            var FormData = require("form-data");
            var form = new FormData();

            form.append("image", part, {filename: part.filename,contentType: part["content-type"]});
            console.log('getting image link');
            // return false;
            var r = request.post("http://localhost/api/uploads", { "headers": {"transfer-encoding": "chunked"} }, function(err, res2, body){ 
                req.link = (JSON.parse(res2.body).link);
                req.size = (JSON.parse(res2.body).size);
                // console.log(JSON.parse(res2.body).link);
                // return false;
                next();
            });
            r._form = form;
        }
    })

    form.on("error", function(error){
        console.log(error);
    })

    form.parse(req);    
}

app.get('/', function (req, res) {
    res.redirect('/home');
});

app.get('/home', function (req, res) {
    res.render('index');
});

app.get('/open-school/', function (req, res) {
    res.render('ocw/oc');
});

app.get('/open-school/dashboard/:user', function (req, res) {
    res.render('ocw/tutor');
    // res.render('ocw/'+req.params.user);
});

app.get('/open-school/tutor/:class', function (req, res) {
    res.render('ocw/tutor-class');
});

app.get('/tutor/exam/:name', function (req, res) {
    res.render('ocw/create-exams');
});

app.get('/logout', function (req, res) {
    res.render('logout');
});

app.get('/me/class', function (req, res) {
	res.render('class');
});

app.get('/programme/:programme', function (req, res) {
	res.render('programme', {p : req.params.programme});
});

app.get('/courses', function (req, res) {
	res.render('courses');
});

app.get('/sign-in', function (req, res) {
	res.render('sign-in');
});

app.get('/programme', function (req, res) {
	res.render('programme');
});

app.get('/donate', function (req, res) {
    res.render('donate');
});

app.get('/faqs', function (req, res) {
    res.render('faqs');
});

app.get('/contact-us', function (req, res) {
    res.render('contact-us');
});

app.get('/about', function (req, res) {
    res.render('about-us');
});

app.get('/classroom/:subject_name', function (req, res) {
    res.render('classroom', {_subName : req.params.subject_name});
});

app.get('/test/:subject_name', function (req, res) {
    res.render('tests', {_subName : req.params.subject_name});
});

app.get('/subject/:subject_name', function (req, res) {
    res.render('course-details', {_subName : req.params.subject_name});
});

app.get('/admin/home', function(req, res) {
    res.render('admin/administrator')
})

app.get('/tutor/home', function(req, res) {
    res.render('teacherFolder/homepage')
})

app.get('/tutor/sign-in', function(req, res) {
    res.render('teacherFolder/sign-in')
})

app.get('/tutor/register', function(req, res) {
    res.render('teacherFolder/register')
})

app.get('/tutor/:sub', function(req, res) {
    res.render('teacherFolder/subject', { sub: req.params.sub})
})

app.get('/admin/subject', function(req, res) {
	res.render('admin/subject')
})

app.get('/admin/programme/:courseName', function(req, res) {
	// console.log()
	// res.send(req.params.courseName)
	res.render('admin/programme', {programme: req.params.courseName})
})

app.get('/my-account', function (req, res) {
	res.render('my-account');
});

app.get('/me/set-up-profile', function (req, res) {
    res.render('my-account');
});

app.get('/course-details', function (req, res) {
    res.render('course-details');
});

app.get('/*', function (req, res) {
    res.render('404');
});

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://churchcastdummy.firebaseio.com"
// });



// const options = {
//   key: fs.readFileSync('./assets/ssl/privateKey.pem'),
//   cert: fs.readFileSync('./assets/ssl/certificate.pem')
// };

// var ref = admin.database().ref('/BREAKING_YOKE');

// async function getDatas (req, res, next) {
// 	ref.child('/Users').on('value', function(snapshot) {
//   		req.data = snapshot.val() ;
// 		next();
// 	});
// }

// async function getPastors (req, res, next) {
// 	ref.child('/Pastors').on('value', function(snapshot) {
//   		req.data = snapshot.val();
// 		next();
// 	});
// }

// async function getUserDetail (req, res, next) {
// 	ref.child('/Users/'+req.params.detail).on('value', function(snapshot) {
//   		req.data = snapshot.val() ;
// 		next();
// 	});
// }

// async function addMember(req, res, next) {
// 	ref.child('/Users').push().set({
// 		name: req.formData.name[0],
// 		email: req.formData.email[0],
// 		gender: req.formData.gender[0],
// 		date_of_birth: req.formData.date_of_birth[0],
// 		occupation: req.formData.occupation[0],
// 		pic: req.link,
// 	});
// 	next();
// }

// async function addAnnouncements(req, res, next) {
// 	console.log('adding announcements');
// 	let key = ref.child('/Announcements').push();
// 	data = {
// 		title: req.formData.title[0],
// 		announcement: req.formData.announcement[0],
// 		date: req.formData.date[0],
// 		tempKey: key.getKey(),
// 		pic: req.link,
// 	};
// 	key.set(data);
// 	// console.log(data);
// 	next();
// }

// https.createServer(options, app).listen(3433);
const port = process.env.PORT || 80
app.listen(port, () => {
    console.log('listening on port ' + port);
});